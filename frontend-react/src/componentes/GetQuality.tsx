interface GetQualityProps {
    imagen1: any
    h2: string
    p: string
}

export default function GetQuality(props: GetQualityProps) {
    return (
        <div className="get-quality">
            <img src={props.imagen1} alt="" />
            <h2>{props.h2}</h2>
            <p>{props.p}</p>
        </div>
    )
}