interface TextoProps {
    letrah6: string
    letrah1: string
    parrafo: string
}
export default function Texto(props: TextoProps) {
    return(
        <div>
            <h6>{props.letrah6}</h6>
            <h1>{props.letrah1}      </h1>
            <p>{props.parrafo}</p>
        </div>
    )
}