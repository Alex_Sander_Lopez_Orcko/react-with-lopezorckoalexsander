import Link from 'next/link'
import Button from './Button'
export default function Header() {
    return (
        <div className='nav-header'>
            <h1>BreadName</h1>
            <ul>
                <li>
                    <Link href="/">Home</Link>
                </li>
                <li>
                    <Link href="/">Product</Link>
                </li>
                <li>
                    <Link href="/">Pricing</Link>
                </li>
                <li>
                    <Link href="/">Contact</Link>
                </li>
            </ul>
            <div>
                <Button title="Login" transparent />
                <Button title="JOIN US --> " />
            </div>
        </div>
    )
}