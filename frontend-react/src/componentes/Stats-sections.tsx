interface StatsSectionsProps {
    letrah1: string
    parrafo: string
}

export default function StatsSections(props: StatsSectionsProps) {
    return (
        <div className="stat-section">
            <div>
                <h1 className="texth-stats">{props.letrah1}</h1>
            </div>
            <div>
                <p className="textp-stats">{props.parrafo}</p>
            </div>
        </div>
    )
}