
interface ButtonProps {
    title: string
    transparent?: boolean
    border?: boolean
}
export default function Button(props: ButtonProps) {
    return (
        props.transparent ?
            <button className={props.border ? "button button-transparent button-border" : "button button-transparent"}>
                {props.title}
            </button>
            :
            <button className={props.border ? "button button-color button-border" : "button button-color"}>
                {props.title}
            </button>
    )
}