import Image from "next/image";
import styles from "./page.module.css";
import Button from "@/componentes/Button";
import Texto from "@/componentes/texto";
import StatsSections from "@/componentes/Stats-sections";
import GetQuality from "@/componentes/GetQuality";

export default function Home() {
  return (
    <main>
      <section className="contenedor">
        <div>
          <div className="letra-contenedor">
            <Texto letrah6="Best dental surgeons" letrah1="25K+ STUDENTS TRUST US" parrafo="Our goal is to make online education work for everyone" />
            <div>
              <Button title="Get Quote Now" />
              <Button title="Learn More" border transparent />
            </div>
          </div>
        </div>
        <div>
          <img className="imagen" src="/imagenes/imagen1.svg" alt="" />
        </div>
      </section>
      <section className="stats">
        <StatsSections letrah1="15K" parrafo="Happy Students" />
        <StatsSections letrah1="150K" parrafo="Monthly Visitors" />
        <StatsSections letrah1="15" parrafo="Monthly Visitors" />
        <StatsSections letrah1="100+" parrafo="Top Partners" />
      </section>
      <section className="section2">
        <div className="letra-contenedor2">
          <Texto letrah6="Practice Advice " letrah1=" Get Quality Education" parrafo="Problems trying to resolve the conflict between the two major realms of Classical physics: Newtonian mechanics " />
        </div>
        <div className="contenedor2">
          <GetQuality imagen1="/imagenes/imagen1-get.svg" h2="Lifetime access" p="The gradualaccumulation of information abou" />
          <GetQuality imagen1="/imagenes/imagen2-get.svg" h2="training Courses" p="The gradualaccumulation of information abou" />
          <GetQuality imagen1="/imagenes/imagen3-get.svg" h2="training Courses" p="The gradualaccumulation of information abou" />
        </div>
      </section>
    </main>
  );
}
